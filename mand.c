/*
 *
 * File:            mandelbrot.cpp
 * Author:          Dany Shaanan
 * Website:         http://danyshaanan.com
 * File location:   https://github.com/danyshaanan/mandelbrot/blob/master/cpp/mandelbrot.cpp
 *
 * Created somewhen between 1999 and 2002
 * Rewritten August 2013
 *
 */

#include <mpi.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

////////////////////////////////////////////////////////////////////////////////

const int MAX_WIDTH_HEIGHT = 28000;
const int HUE_PER_ITERATION = 5;

////////////////////////////////////////////////////////////////////////////////

class State {
    public:
        double centerX;
        double centerY;
        double zoom;
        int maxIterations;
        long w;
        long h;
        State() {
            //centerX = -.75;
            //centerY = 0;
            centerX = -1.186340599860225;
            centerY = -0.303652988644423;
            zoom = 400;
            maxIterations = 100;
            w = 28000;
            h = 28000;
        }
};

////////////////////////////////////////////////////////////////////////////////

float iterationsToEscape(double x, double y, int maxIterations) {
    double tempa;
    double a = 0;
    double b = 0;
    int i;

    for (i = 0 ; i < maxIterations ; i++) {
        tempa = a*a - b*b + x;
        b = 2*a*b + y;
        a = tempa;
        if (a*a+b*b > 64) {
            // return i; // discrete
            return i - log(sqrt(a*a+b*b))/log(8); //continuous
        }
    }
    return -1;
}

int hue2rgb(float t){
    while (t>360) {
        t -= 360;
    }
    if (t < 60) return 255.*t/60.;
    if (t < 180) return 255;
    if (t < 240) return 255. * (4. - t/60.);
    return 0;
}

void writeImage(unsigned char *img, int w, int h) {
    long long filesize = 54 + 3*(long long)w*(long long)h;
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);

    FILE *f;
    f = fopen("temp.bmp","wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    for (int i=0; i<h; i++) {
        long long offset = ((long long)w*(h-i-1)*3);
        fwrite(img+offset,3,w,f);
        fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }
    fclose(f);
}

unsigned char *createImage(State state, unsigned char *img, int startRow, int endRow) {
    int w = state.w;
    int h = state.h;

    if (w > MAX_WIDTH_HEIGHT) w = MAX_WIDTH_HEIGHT;
    if (h > MAX_WIDTH_HEIGHT) h = MAX_WIDTH_HEIGHT;

    unsigned char r, g, b;

    double xs[MAX_WIDTH_HEIGHT], ys[MAX_WIDTH_HEIGHT];

    // init xs and ys arrays
    // with values ?
    int px, py;
    for (px=0; px<w; px++) {
        xs[px] = (px - w/2)/state.zoom + state.centerX;
    }
    for (py=0; py<h; py++) {
        ys[py] = (py - h/2)/state.zoom + state.centerY;
    }

    int loc = 0;
    for (px=0; px<w; px++) {
        for (py=startRow; py<endRow; py++) {
            r = g = b = 0;
            float iterations = iterationsToEscape(xs[px], ys[py], state.maxIterations);
            if (iterations != -1) {
                float h = HUE_PER_ITERATION * iterations;
                r = hue2rgb(h + 120);
                g = hue2rgb(h);
                b = hue2rgb(h + 240);
            }
            img[loc+2] = (unsigned char)(r);
            img[loc+1] = (unsigned char)(g);
            img[loc+0] = (unsigned char)(b);
            loc += 3;
        }
    }
    return img;
}

////////////////////////////////////////////////////////////////////////////////

unsigned char *MasterProcess(State state, int NUM_ROWS)
{
    long i, done;
    int flag, source;
    MPI_Status status;
    int nproc;

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    // printf("%d: The master process is alive and sees %d slaves\n", __LINE__, nproc-1);

    int startRow = 0;
    int NUM_ELEMENTS = NUM_ROWS * state.w * 3;
    long positions[16];

    for (i=1 ; i<nproc ; ++i) {
        MPI_Send(&startRow, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        positions[i] = startRow;
        startRow += NUM_ROWS;
    }


    // Get the results back from the slaves in no particular order
    done = 0;
    int numKiled = 0;
    unsigned char *img = (unsigned char *)malloc(3*state.w*state.h);
    unsigned long offset;

    while (!done)
    {
        // see if any messages have been sent back
        MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
        if (flag)
        {
            source = status.MPI_SOURCE;
            // printf("%d: Received from source %d\n", __LINE__, source);
            
            offset = positions[source] * state.w * 3;
            MPI_Recv(&img[offset], NUM_ELEMENTS, MPI_UNSIGNED_CHAR, source, 0, MPI_COMM_WORLD, &status);
            
            if (startRow >= state.w) { //if done, send a poison pill
                // printf("%d: Killing %d\n", __LINE__, source);
                int kill = -1;
                MPI_Send(&kill, 1, MPI_INT, source, 0, MPI_COMM_WORLD);
                if (++numKiled >= nproc - 1) {
                    done=1;
                }
            } else {
                // printf("%d: Sending more work to %d\n", __LINE__, source);
                MPI_Send(&startRow, 1, MPI_INT, source, 0, MPI_COMM_WORLD);
                positions[source] = startRow;
                startRow += NUM_ROWS;
            }
        }
    }

    return img;
}


void SlaveProcess(State state, int NUM_ROWS)
{
    int done=0;
    int source=0;
    MPI_Status status;

    int rowStart;

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    unsigned char *img = (unsigned char *)malloc(NUM_ROWS * state.w * 3);

    while (1) {
        MPI_Recv(&rowStart, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &status);
        
        if (rowStart == -1) {
            break;
        }

        int endRow = rowStart + NUM_ROWS <= state.w ? rowStart + NUM_ROWS : state.w;

        // printf("%d: Rank %d started [%d - %d] row\n", __LINE__, rank, rowStart, endRow);

        img = createImage(state, img, rowStart, endRow);

        // printf("%d: Rank %d finished [%d - %d] row\n", __LINE__, rank, rowStart, endRow);

        MPI_Send(img, NUM_ROWS * state.w * 3, MPI_UNSIGNED_CHAR, source, 0, MPI_COMM_WORLD);

    }
    free(img);

    // printf("%d: Proc %d killed\n", __LINE__, rank);
}

double When()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

void draw(State state, int NUM_ROWS) {

    int rank;
    int work_location = 0;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); //set initial rank
    
    if (rank == 0) {
        double starttime = When();
        unsigned char *img = MasterProcess(state, NUM_ROWS);
        double endtime = When();
        fprintf(stderr, "%d: time = %lf for size %d\n", __LINE__, endtime - starttime, NUM_ROWS);
        writeImage(img, state.w, state.h);
        free(img);
    } else {
        SlaveProcess(state, NUM_ROWS);
    }


}

int main(int argc, char **argv) {

    MPI_Init(&argc, &argv); //init MPI
    int NUM_ROWS = atoi(argv[1]);

    State state;
    draw(state, NUM_ROWS);
    
    MPI_Finalize();

    return 0;
}